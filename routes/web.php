<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MasukController;
use App\Http\Controllers\KeluarController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [LoginController::class, 'index'])->name('login');

//login

Route::post('/login', [LoginController::class, 'authenticate']);
Route::get('/logout', [LoginController::class, 'logout']);

//register
Route::get('/register', [RegisterController::class, 'index']);
Route::post('/register', [RegisterController::class, 'storeg']);

Route::group(['middleware' =>['auth']], function(){

//dashboard
Route::get('/dashboard', [DashboardController::class, 'index']);


//masuk
Route::get('/show', [MasukController::class, 'show']);
Route::get('/tambahsurat', [MasukController::class, 'tambahsurat']);
Route::post('/uploadsurat', [MasukController::class, 'store']);
Route::get('/Masuk/{id}/edit', [MasukController::class, 'edit']);
Route::post('/Masuk/{id}/update', [MasukController::class, 'update']);
Route::get('/Masuk/{id}/destroy', [MasukController::class, 'destroy']);
Route::get('/download/{file}', [MasukController::class, 'download']);
Route::get('/view/{id}', [MasukController::class, 'view']);
Route::get('/laporan', [MasukController::class, 'laporan']);

//keluar
Route::get('/show2', [KeluarController::class, 'show2']);
Route::get('/tambahsurat2', [KeluarController::class, 'tambahsurat2']);
Route::post('/uploadsurat2', [KeluarController::class, 'store2']);
Route::get('/Keluar/{id}/edit2', [KeluarController::class, 'edit2']);
Route::post('/Keluar/{id}/update2', [KeluarController::class, 'update2']);
Route::get('/Keluar/{id}/destroy', [KeluarController::class, 'destroy']);
Route::get('/download2/{file}', [KeluarController::class, 'download2']);
Route::get('/view2/{id}', [KeluarController::class, 'view2']);
Route::get('/laporan2', [KeluarController::class, 'laporan2']);
});
