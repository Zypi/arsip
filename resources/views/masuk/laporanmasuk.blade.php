@extends('layouts.master')
@section('content')
<!-- Begin Page Content -->
<div class="main-content">
        <section class="section">
          <div class="section-body">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Laporan Surat Masuk</h4>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped table-hover" id="save-stage" style="width:100%;">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>No Surat</th>
                            <th>Lampiran</th>
                            <th>Perihal</th>
                            <th>Tgl Surat</th>
                            <th>Tembusan</th>
                          </tr>
                        </thead>
                        <tbody>
                        @php $no=1; @endphp
                        @foreach($data as $data)
                          <tr>
                            <td>{{$no++}}</td>
                            <td>{{$data->no_surat}}</td>
                            <td>{{$data->lampiran}}</td>
                            <td>{{$data->perihal}}</td>
                            <td>{{$data->tgl_surat}}</td>
                            <td>{{$data->tembusan}}</td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
@endsection