<div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="#"> <img alt="image" src="{{asset('assets/img/dnbs.jpg')}}" class="header-logo" /> <span
                class="logo-name">E-Arsip</span>
            </a>
          </div>
          <ul class="sidebar-menu">
            
            <li class="dropdown active">
              <a href="{{url('dashboard')}}" class="nav-link"><i data-feather="monitor"></i><span>Dashboard</span></a>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i
                  data-feather="list"></i><span>Data Master</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{url('show')}}">Surat Masuk</a></li>
                <li><a class="nav-link" href="{{url('show2')}}">Surat Keluar</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="list"></i><span>Data Laporan</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{url('laporan')}}">Surat Masuk</a></li>
                <li><a class="nav-link" href="{{url('laporan2')}}">Surat Keluar</a></li>
              </ul>
            </li>
          </ul>
        </aside>
      </div>