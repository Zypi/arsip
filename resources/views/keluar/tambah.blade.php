@extends('layouts.master')
@section('content')
<div class="main-content">
        <section class="section">
          <div class="section-body">
            <div class="row">
              <div class="col-12 col-md-6 col-lg-6">
              <div class="card">
                  <div class="card-header">
                    <h4>Tambah Data Surat Keluar</h4>
                  </div>
                  <div class="card-body">
                  <form action="{{url('uploadsurat2')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                      <label>Nomor Surat</label>
                      <input type="text" class="form-control" name="no_surat" id="no_surat" value="{{old('no_surat')}}" placeholder="no surat">
                    </div>
                    <div>
                      @error('no_surat')
                      <span store="color:red">{{$message}}</span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label>Lampiran</label>
                      <input type="text" class="form-control" name="lampiran" id="lampiran" value="{{old('lampiran')}}" placeholder="lampiran">
                    </div>
                    <div>
                      @error('lampiran')
                      <span store="color:red">{{$message}}</span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label>Perihal</label>
                      <input type="text" class="form-control" name="perihal" id="perihal"  value="{{old('perihal')}}" placeholder="perihal">
                    </div>
                    <div>
                      @error('perihal')
                      <span store="color:red">{{$message}}</span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label>Tgl Surat</label>
                      <input type="date" class="form-control" name="tgl_surat" id="tgl_surat" value="{{old('tgl_surat')}}">
                    </div>
                    <div>
                      @error('tgl_surat')
                      <span store="color:red">{{$message}}</span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label>Tembusan</label>
                      <select class="form-control" name="tembusan" id="tembusan" value="{{old('tembusan')}}">
                        <option value="">--Pilih Tembusan--</option>
                        <option value="Yayasan">Yayasan</option>
                        <option value="Pertinggal">Pertinggal</option>
                        <option value="Arsip">Arsip</option>
                      </select>
                    </div>
                    <div>
                      @error('tembusan')
                      <span store="color:red">{{$message}}</span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label>File</label>
                      <input type="file" class="form-control" name="file" id="file" value="{{old('file')}}">
                    </div>
                    <div>
                      @error('file')
                      <span store="color:red">{{$message}}</span>
                      @enderror
                    </div>
                  <div>
                    <a href="{{url('show')}}" class="btn btn-outline-primary float-right">kembali</a>
                    <input type="submit" name="tambah" value="tambah"class="btn btn-outline-primary float-left"></button>
                  <form>
                  </div>
              </div>
            </div>
          </div>
</section>
</div>

                
@endsection
