@extends('layouts.master')
@section('content')
<!-- Begin Page Content -->
<div class="main-content">
        <section class="section">
          <div class="section-body">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Table Surat keluar</h4>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped table-hover" id="save-stage" style="width:100%;">
                        <a button type="button" class="btn btn-outline-primary" href="{{url('tambahsurat2')}}"><i class="fa fa-plus">Tambah Data</i></a><br><br>
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>No Surat</th>
                            <th>Lampiran</th>
                            <th>Perihal</th>
                            <th>Tgl Surat</th>
                            <th>Tembusan</th>
                            <th>View</th>
                            <th>Download</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                        @php $no=1; @endphp
                        @foreach($data as $data)
                          <tr>
                            <td>{{$no++}}</td>
                            <td>{{$data->no_surat}}</td>
                            <td>{{$data->lampiran}}</td>
                            <td>{{$data->perihal}}</td>
                            <td>{{$data->tgl_surat}}</td>
                            <td>{{$data->tembusan}}</td>
                            <td><a href="{{url('/view2',$data->id)}}" ><button type="button" class="btn btn-outline-primary"><i class="fa fa-eye">&nbsp;view</button></i></a></td>
                            <td><a href="{{url('/download2',$data->file)}}"><button type="button" class="btn btn-outline-primary"><i class="fa fa-download">&nbsp;Download</button></i></a></td>
                            <td>
                                <a href="/Keluar/{{$data->id}}/edit2" class="btn btn-outline-primary" onclick="return confirm('edit data')"><i class="fa fa-edit"></i></a>
                                <a href="/Keluar/{{$data->id}}/destroy" class="btn btn-outline-danger" onclick="return confirm('hapus data')"><i class="fa fa-trash"></i></a>
                            </td> 
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
@endsection