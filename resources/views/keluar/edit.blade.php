@extends('layouts.master')
@section('content')
<div class="main-content">
        <section class="section">
          <div class="section-body">
            <div class="row">
              <div class="col-12 col-md-6 col-lg-6">
              <div class="card">
                  <div class="card-header">
                    <h4>Edit Data Surat Keluar</h4>
                  </div>
                  <div class="card-body">
                  <form action="/Keluar/{{$data->id}}/update2" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                      <label>Nomor Surat</label>
                      <input type="text" class="form-control" name="no_surat" id="no_surat" value="{{$data->no_surat}}">
                    </div>
                    <div class="form-group">
                      <label>Lampiran</label>
                      <input type="text" class="form-control" name="lampiran" id="lampiran" value="{{$data->lampiran}}">
                    </div>
                    <div class="form-group">
                      <label>Perihal</label>
                      <input type="text" class="form-control" name="perihal" id="perihal" value="{{$data->perihal}}">
                    </div>
                    <div class="form-group">
                      <label>Tgl Surat</label>
                      <input type="date" class="form-control" name="tgl_surat" id="tgl_surat" value="{{$data->tgl_surat}}">
                    </div>
                    <div class="form-group">
                      <label>Tembusan</label>
                      <select class="form-control" name="tembusan" id="tembusan">
                        <option value="Yayasan" @if($data->tembusan =='Yayasan') selected @endif >Yayasan</option>
                        <option value="Pertinggal" @if($data->tembusan =='Pertinggal') selected @endif >Pertinggal</option>
                        <option value="Arsip" @if($data->tembusan =='Arsip') selected @endif >Arsip</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>File</label>
                        </br>
                      <iframe height="100" width="100" src="/assets/{{$data->file}}" ></iframe></br></br>
                      <input type="file" class="form-control" name="file" id="file" value="{{$data->file}}">
                    </div>
                    <a href="{{url('show2')}}" class="btn btn-outline-primary float-right">kembali</a>
                    <input type="submit" name="update" value="update"class="btn btn-outline-primary float-left"></button>
                  </div>
                </form>
                </div>
</div>
</section>
</div>

                
@endsection
