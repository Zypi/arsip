<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function index(){
        return view('register.index',[
            'title'=>'register',
            'active'=>'register'
        ]);
    }
    public function storeg(Request $request){
        $validateData=$request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email:dns|unique:users',
            'password' => 'required|min:6|max:255'
        ]);

        $validateData['password'] = Hash::make($validateData['password']);

        User::create($validateData);

      //  $request->session()->flash('succes', 'Registration Successfull! Please login');

        return redirect('/')->with('success', 'Registration Successfull! Please login');
    }
}
