<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Stroage;
use App\Models\Keluar;


class KeluarController extends Controller
{
    public function show2()
    {
        $data=keluar::all();
        return view('keluar.suratkeluar',compact('data'));
    }
    public function tambahsurat2(){
        return view('keluar.tambah');
    }
    public function store2(Request $request){
        $request->validate([
            'no_surat'=>'required',
            'lampiran'=>'required',
            'perihal'=>'required',
            'tgl_surat'=>'required',
            'tembusan'=>'required',
            'file'=>'required',
        ]);
        $data=new keluar();

        $file=$request->file;
        $filename=time().'.'.$file->getClientOriginalExtension();
        $request->file->move('assets',$filename);
        $data->file=$filename;

        $data->no_surat=$request->no_surat;
        $data->lampiran=$request->lampiran;
        $data->perihal=$request->perihal;
        $data->tgl_surat=$request->tgl_surat;
        $data->tembusan=$request->tembusan;
        
        $data->save();
        return redirect('show2')->with('store2');
    }
    public function download2(Request $request,$file){
        return response()->download(public_path('assets/'.$file));
    }
    public function view2($id){
        $data=keluar::find($id);
        return view('keluar.view',compact('data'));
    }
    public function edit2($id){
        $data=keluar::find($id);
        return view('keluar.edit',compact('data'));
        
    }
    public function update2(Request $request, $id){
        $data=keluar::find($id);
        if($request->file('file'))
        {
            $file=$request->file;
            $filename=time().'.'.$file->getClientOriginalExtension();
            $request->file->move('assets',$filename);
            $data->file=$filename;
        }
        $data->no_surat=$request->no_surat;
        $data->lampiran=$request->lampiran;
        $data->perihal=$request->perihal;
        $data->tgl_surat=$request->tgl_surat;
        $data->tembusan=$request->tembusan;

        $data->save();
        return redirect('show2')->with('edit2');
    }
    public function laporan2(){
        $data=keluar::all();
        return view('keluar.laporankeluar',compact('data'));
    }
    public function destroy($id){

        $data=keluar::find($id);
        $data->delete();
        return redirect('show2')->with('Berhasil','Data Telah Dihapus');
    }
}
