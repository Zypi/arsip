<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Stroage;
use App\Models\Masuk;

class MasukController extends Controller
{
    public function show(){
        $data=masuk::all();
        return view('masuk.suratmasuk',compact('data'));
    }
    public function tambahsurat(){
        return view('masuk.tambah');
    }
    public function store(Request $request){
        $request->validate([
            'no_surat'=>'required',
            'lampiran'=>'required',
            'perihal'=>'required',
            'tgl_surat'=>'required',
            'tembusan'=>'required',
            'file'=>'required',
        ]);
        $data=new masuk();

        $file=$request->file;
        $filename=time().'.'.$file->getClientOriginalExtension();
        $request->file->move('assets',$filename);
        $data->file=$filename;

        $data->no_surat=$request->no_surat;
        $data->lampiran=$request->lampiran;
        $data->perihal=$request->perihal;
        $data->tgl_surat=$request->tgl_surat;
        $data->tembusan=$request->tembusan;
        
        $data->save();
        return redirect('show')->with('store');
    }
    public function download(Request $request,$file){
        return response()->download(public_path('assets/'.$file));
    }
    public function view($id){
        $data=masuk::find($id);
        return view('masuk.view',compact('data'));
    }
    public function edit($id){
        $data=masuk::find($id);
        return view('masuk.edit',compact('data'));
        
    }
    public function update(Request $request, $id){
        $data=masuk::find($id);
        if($request->file('file'))
        {
            $file=$request->file;
            $filename=time().'.'.$file->getClientOriginalExtension();
            $request->file->move('assets',$filename);
            $data->file=$filename;
        }
        $data->no_surat=$request->no_surat;
        $data->lampiran=$request->lampiran;
        $data->perihal=$request->perihal;
        $data->tgl_surat=$request->tgl_surat;
        $data->tembusan=$request->tembusan;

        $data->save();
        return redirect('show')->with('edit');
    }
    public function destroy($id){

        $data=masuk::find($id);
        $data->delete();
        return redirect('show')->with('Berhasil','Data Telah Dihapus');
    }
    public function laporan(){
        $data=masuk::all();
        return view('masuk.laporanmasuk',compact('data'));
    }

}
