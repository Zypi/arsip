<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Masuk;
use App\Models\Keluar;

class DashboardController extends Controller
{
    public function index(){
        $jumlah_file = masuk::all()->count();
        $jml_file = keluar::all()->count();
        return view('dashboard.index',compact('jumlah_file', 'jml_file'));
        
    }
}
