<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Masuk extends Model
{
    use HasFactory;
    protected $table="masuks";
    protected $primarykey="id";
    protected $fillable=['id','no_surat','lampiran','perihal','tgl_surat','tembusan','file'];
   
}
