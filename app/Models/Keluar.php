<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Keluar extends Model
{
    use HasFactory;
    protected $table="keluars";
    protected $primarykey="id";
    protected $fillable=['id','no_surat','lampiran','perihal','tgl_surat','tembusan','file'];
   
}
